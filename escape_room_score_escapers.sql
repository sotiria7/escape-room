CREATE DATABASE  IF NOT EXISTS `escape_room_score` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `escape_room_score`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: escape_room_score
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `escapers`
--

DROP TABLE IF EXISTS `escapers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escapers` (
  `idEscaper` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EscaperUserName` varchar(45) NOT NULL,
  `EscaperPassword` varchar(45) NOT NULL,
  `EscaperScore` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idEscaper`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escapers`
--

LOCK TABLES `escapers` WRITE;
/*!40000 ALTER TABLE `escapers` DISABLE KEYS */;
INSERT INTO `escapers` VALUES (1,'wqe@we.com','wef',0),(2,'efq@we.COM','WEF',0),(3,'sot@gmail.com','er',0),(4,'ws@google.com','weR',0),(5,'gws@erayg.com','w\\r',0),(6,'srt@gmail.com','wef',0),(7,'efw@AD.gr','qwef',0),(8,'john@gmail.com','we',0),(9,'wadef@EF.com','wse',0),(10,'qE@WEF.COM','WE',0),(11,'qr@er.com','we',0),(12,'clo@we.grplpl','plpl',0),(13,'efw@re.com','TR',0),(14,'sotr@gmail.com','we',0),(15,'maria@gmail.com','wer',0),(16,'qRq@eotij.com','pwerjt',0),(17,'oiu8@;oi.com','oij',0),(18,'oiu8@oi.com','oij',0),(19,'WER@TH.COM','RE',0),(20,'sotr@gmail.com','sotr',0),(21,'poi@poij.com','er',0),(22,'ewr@er','54w',0),(23,'ewr@er.com','54w',0),(24,'sof@gmail.com','sof',0),(25,'sog@foo.gr','sog',0),(26,'sot@foo.gr','sot',0),(27,'sotr@do.com','sotr',0),(28,'sofd@er.com','aer',0),(29,'man@lk.com','man',0),(30,'load@fr','fr',0),(31,'load@fr.com','fr',0),(32,'rea@gmail.com','rea',0),(33,'maria@hot.fe','maria',0),(34,'maria@m.com','maria',0),(35,'twra@foo.gr','twra',0),(36,'jdk@ed.com','jdk',0),(37,'maria','maria',0),(38,'maria@hot.gr','maria',0),(39,'sofd@er.com','uy',0);
/*!40000 ALTER TABLE `escapers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-04 19:24:55